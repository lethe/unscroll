# unscroll

Takes a `scroll://` URL, fetches it, and dumps the result to `stdout`.

Forked from [gcat](https://github.com/aaronjanse/gcat).

Symlink `unscroll` anywhere in your $PATH to begin.

I only recommend using it for plaintext files.